package cl.softegrity.poc.empty.java.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmptyJavaSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmptyJavaSpringBootApplication.class, args);
	}

}
