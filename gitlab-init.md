# Command line instructions
Previous:
- Create project with STS
- Create empty project in GitLab without Initialize repository with a README

You can also upload existing files from your computer using the instructions below.

## Git global setup
```
git config --global user.name "Name"
git config --global user.email "email"
```

## Create a new repository
```
git clone https://gitlab.com/jorge.gajardo/empty-java-spring-boot.git
cd empty-java-spring-boot
git switch -c master
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
```

## Push an existing folder
```
cd existing_folder
git init --initial-branch=master
git remote add origin https://gitlab.com/jorge.gajardo/empty-java-spring-boot.git
git add .
git commit -m "First commit"
git push -u origin master
```

## Push an existing Git repository
```
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/jorge.gajardo/empty-java-spring-boot.git
git push -u origin --all
git push -u origin --tags
```